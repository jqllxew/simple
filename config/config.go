package config

import (
	"github.com/kataras/golog"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var instance = &Config{}

type (
	Simple struct {
		Port          string `yaml:"Port"`          // 端口
		LogFile       string `yaml:"LogFile"`       // 日志文件位置
		ShowSql       bool   `yaml:"ShowSql"`       // 是否显示sql
		ShowAuthLog   bool   `yaml:"ShowAuthLog"`   // 是否显示权限判断
		LogLevel      string `yaml:"LogLevel"`      // 日志等级
		DBDialect     string `yaml:"DBDialect"`     // 数据库方言
		DBUrl         string `yaml:"DBUrl"`         // 数据库连接地址
		DBMaxIdle     int    `yaml:"MaxIdle"`       // 最大闲置连接
		DBMaxOpen     int    `yaml:"MaxOpen"`       // 最大打开连接
		TokenKey      string `yaml:"TokenKey"`      // token
		TokenSecond   int    `yaml:"TokenSecond"`   // Token时长单位 秒
		UploadType    string `yaml:"UploadType"`    // aliOss、txCos 或 local
		UploadAddHost bool   `yaml:"UploadAddHost"` // 上传返回是否拼接host
		AuthRoot      string `yaml:"AuthRoot"`      // 权限账号
	}
	AliOss struct {
		Host         string `yaml:"Host"`
		Bucket       string `yaml:"Bucket"`
		Endpoint     string `yaml:"Endpoint"`
		AccessId     string `yaml:"AccessId"`
		AccessSecret string `yaml:"AccessSecret"`
	}
	TxCos struct {
		Host      string `yaml:"Host"`
		Region    string `yaml:"Region"`
		SecretId  string `yaml:"SecretId"`
		SecretKey string `yaml:"SecretKey"`
	}
	Local struct {
		Host string `yaml:"Host"`
		Path string `yaml:"Path"`
	}
	Config struct {
		Simple Simple `yaml:"Simple"`
		AliOss AliOss `yaml:"AliOss"`
		TxCos  TxCos  `yaml:"TxCos"`
		Local  Local  `yaml:"Local"`
	}
)

func Init(filename string) {
	if filename == "" {
		golog.Info("未设置yaml文件路径，全部参数将使用默认值")
	} else if yamlFile, err := ioutil.ReadFile(filename); err != nil {
		golog.Error("请检查yaml文件路径", err)
	} else if err = yaml.Unmarshal(yamlFile, instance); err != nil {
		golog.Error(err)
	}
	if instance.Simple.Port == "" {
		instance.Simple.Port = "8080"
	}
	if instance.Simple.LogFile == "" {
		instance.Simple.LogFile = "./data/logs/"
	}
	if instance.Simple.LogLevel == "" {
		instance.Simple.LogLevel = "info"
	}
	if instance.Simple.DBDialect == "" {
		instance.Simple.DBDialect = "mysql"
	}
	if instance.Simple.DBMaxIdle == 0 {
		instance.Simple.DBMaxIdle = 10
	}
	if instance.Simple.DBMaxOpen == 0 {
		instance.Simple.DBMaxOpen = 20
	}
	if instance.Simple.TokenKey == "" {
		instance.Simple.TokenKey = "Authorized"
	}
	if instance.Simple.TokenSecond == 0 {
		instance.Simple.TokenSecond = 3600
	}
	if instance.Simple.UploadType == "" {
		instance.Simple.UploadType = "local"
	}
	if instance.Simple.AuthRoot == "" {
		instance.Simple.AuthRoot = "admin"
	}
}
func Instance() *Config {
	return instance
}
func GetSimple() Simple {
	return Instance().Simple
}
