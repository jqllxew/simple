package simple

import (
	"encoding/json"
	"fmt"
	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
	"reflect"
	"strconv"
	"strings"
)

func JsonParams(ctx iris.Context) *JsonMap {
	body, _ := ctx.GetBody()
	params := make(map[string]interface{})
	err := json.Unmarshal(body, &params)
	if err != nil {
		golog.Error(err)
	}
	return &JsonMap{m: params}
}

func URLParams(ctx iris.Context) *JsonMap {
	params := ctx.URLParams()
	p := make(map[string]interface{})
	for k, v := range params {
		p[k] = v
	}
	return &JsonMap{m: p}
}

func FormParams(ctx iris.Context) *JsonMap {
	params := ctx.FormValues()
	p := make(map[string]interface{})
	for k, v := range params {
		p[k] = strings.Join(v, ",")
	}
	return &JsonMap{m: p}
}

type JsonMap struct {
	m map[string]interface{}
}

func (j *JsonMap) Set(key string, val interface{}) {
	if j.m == nil {
		j.m = make(map[string]interface{})
	}
	j.m[key] = val
}

func (j *JsonMap) Get(key string) interface{} {
	return j.m[key]
}

func (j *JsonMap) GetString(key string) string {
	val := j.Get(key)
	if val != nil {
		return val.(string)
	}
	return ""
}

func (j *JsonMap) GetMap() map[string]interface{} {
	return j.m
}

func (j *JsonMap) GetInt(key string) int64 {
	val := j.Get(key)
	ty := reflect.TypeOf(val).Name()
	if ty == "string" {
		intVal, err := strconv.ParseInt(val.(string), 10, 64)
		if err != nil {
			golog.Error(err)
		}
		return intVal
	} else if ty == "int64" {
		return val.(int64)
	} else if ty == "float64" {
		f := val.(float64)
		fs := fmt.Sprintf("%.0f", f)
		intVal, err := strconv.ParseInt(fs, 10, 64)
		if err != nil {
			golog.Error(err)
		}
		return intVal
	}
	golog.Error("getInt but type:", ty)
	return 0
}

func (j *JsonMap) ContainsKey(key string) bool {
	_, ok := j.m[key]
	return ok
}
