module gitee.com/jqllxew/simple

go 1.16

require (
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/iris-contrib/middleware/cors v0.0.0-20210110101738-6d0a4d799b5d
	github.com/jinzhu/gorm v1.9.16
	github.com/kataras/golog v0.1.7
	github.com/kataras/iris/v12 v12.2.0-alpha2
	github.com/robfig/cron v1.2.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	golang.org/x/text v0.3.5
	gopkg.in/yaml.v2 v2.4.0
)
