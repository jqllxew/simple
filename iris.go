package simple

import (
	"gitee.com/jqllxew/simple/config"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/recover"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func InitIris(hub func(app *iris.Application)) {
	conf := config.GetSimple()
	app := iris.New()
	app.Logger().SetLevel(conf.LogLevel)
	app.Use(recover.New())
	app.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, // allows everything, use that to change the hosts.
		AllowCredentials: true,
		MaxAge:           3600,
		AllowedMethods:   []string{iris.MethodGet, iris.MethodPost, iris.MethodOptions, iris.MethodHead, iris.MethodDelete, iris.MethodPut},
		AllowedHeaders:   []string{"*"},
	}))
	app.AllowMethods(iris.MethodOptions)
	if hub != nil {
		hub(app)
	}
	server := &http.Server{Addr: ":" + conf.Port}
	handleSignal(server)
	err := app.Run(iris.Server(server), iris.WithConfiguration(iris.Configuration{
		DisableStartupLog:                 false,
		DisableInterruptHandler:           false,
		DisablePathCorrection:             false,
		EnablePathEscape:                  false,
		FireMethodNotAllowed:              false,
		DisableBodyConsumptionOnUnmarshal: false,
		DisableAutoFireStatusCode:         false,
		EnableOptimizations:               true,
		TimeFormat:                        "2006-01-02 15:04:05",
		Charset:                           "UTF-8",
	}))
	if err != nil {
		golog.Error(err)
		os.Exit(-1)
	}
}
func handleSignal(server *http.Server) {
	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGTERM)
	go func() {
		s := <-c
		golog.Infof("got signal [%s], exiting now", s)
		CloseDB()
		StopCron()
		if err := server.Close(); nil != err {
			golog.Errorf("close failed: " + err.Error())
		}
		golog.Infof("Exited")
		os.Exit(0)
	}()
}
