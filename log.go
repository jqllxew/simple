package simple

import (
	"fmt"
	"gitee.com/jqllxew/simple/config"
	"github.com/kataras/golog"
	"os"
	"runtime"
	"strings"
	"time"
)

var (
	logFile *os.File
)

func InitLog() {
	golog.SetLevel(config.GetSimple().LogLevel)
	golog.SetTimeFormat("15:04:05")
	golog.Handle(func(l *golog.Log) bool {
		prefix := golog.GetTextForLevel(l.Level, true)
		message := fmt.Sprintf("%s %s %s",
			prefix, l.FormatTime(), l.Message)
		if l.NewLine {
			message += "\n"
		}
		fmt.Print(message)
		_, _ = getWriter().WriteString(message)
		return true
	})
	_ = StartCron("@midnight", func() {
		if logFile != nil {
			_ = logFile.Close()
			logFile = nil
		}
	})
}

// https://golang.org/doc/go1.9#callersframes
func getCaller() (string, int) {
	var pcs [10]uintptr
	n := runtime.Callers(3, pcs[:])
	frames := runtime.CallersFrames(pcs[:n])
	for {
		frame, more := frames.Next()
		funcName := frame.Func.Name()
		if (strings.Contains(frame.File, "_examples")) &&
			!strings.Contains(funcName, "app.getCaller") {
			return frame.File, frame.Line
		}
		if !more {
			break
		}
	}
	return "?", 0
}
func getWriter() *os.File {
	conf := config.GetSimple()
	exists, _ := PathExists(conf.LogFile)
	if logFile == nil || !exists {
		file := conf.LogFile + time.Now().Format("2006-01-02") + ".log"
		_ = os.MkdirAll(conf.LogFile, os.ModePerm)
		logFile, _ = os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0766)
	}
	return logFile
}
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
