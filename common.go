package simple

import (
	"encoding/csv"
	"os"
	"sync"
	"time"
)

func Async(ch chan interface{}, f func(param interface{})) chan interface{} {
	if ch == nil {
		ch = make(chan interface{})
	}
	go func(c chan interface{}) {
		for {
			select {
			case param, open := <-c:
				if open {
					f(param)
				} else {
					c = nil
					return
				}
			case <-time.After(time.Minute):
				return
			}
		}
	}(ch)
	return ch
}
func GetCsvWriter(filePath string) (*os.File, *csv.Writer) {
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, os.FileMode(0666))
	if err != nil {
		panic(err)
	}
	writer := csv.NewWriter(file)
	return file, writer
}
func CsvWriter(cw *csv.Writer, val ...string) {
	_ = cw.Write(val)
	cw.Flush()
}

type CInt struct {
	i int
	sync.RWMutex
}

func (ci *CInt) Add() {
	ci.Lock()
	ci.i++
	ci.Unlock()
}
func (ci *CInt) Minus() {
	ci.Lock()
	ci.i--
	ci.Unlock()
}
func (ci *CInt) Get() int {
	ci.RLock()
	i := ci.i
	ci.RUnlock()
	return i
}
