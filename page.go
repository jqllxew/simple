package simple

import (
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris/v12"
	"strconv"
)

// --- page ---
type NormalPage struct {
	Page      int64       `json:"page"`      //当前页
	Size      int64       `json:"size"`      //显示条数
	TotalPage int64       `json:"totalPage"` //总页数
	TotalSize int64       `json:"totalSize"` //总条数
	Params    *JsonMap    `json:"-"`
	DB        *gorm.DB    `json:"-"`
	List      interface{} `json:"list"` //分页数据
}

func (np *NormalPage) pageLimit() (limit int64, start int64) {
	start = (np.Page - 1) * np.Size
	limit = np.Size
	return
}

func PageCreate(ctx iris.Context) *NormalPage {
	page, _ := strconv.ParseInt(ctx.FormValue("page"), 10, 64)
	size, _ := strconv.ParseInt(ctx.FormValue("size"), 10, 64)
	if page < 1 {
		page = 1
	}
	if size < 1 {
		size = 10
	}
	params := URLParams(ctx)
	return &NormalPage{
		Page:   page,
		Size:   size,
		Params: params,
	}
}

func (np *NormalPage) PageSetCount(totalSize int64) (limit int64, start int64) {
	np.TotalSize = totalSize
	np.TotalPage = (np.TotalSize-1)/np.Size + 1
	if np.Page > np.TotalPage {
		np.Page = np.TotalPage
	}
	return np.pageLimit()
}

func (np *NormalPage) PageSetList(list interface{}) {
	np.List = list
}

func (np *NormalPage) Like(column string) *NormalPage {
	return np.Where(column+" like concat('%', ?, '%')", column)
}

func (np *NormalPage) Eq(column string) *NormalPage {
	return np.Where(column+" = ?", column)
}

func (np *NormalPage) Where(query, column string) *NormalPage {
	if np.DB == nil {
		np.DB = DB()
	}
	val := np.Params.Get(column)
	if val != nil && val != "" {
		np.DB = np.DB.Where(query, val)
	}
	return np
}

func (np *NormalPage) Desc(column, defCol string) *NormalPage {
	return np.Order(column, defCol+" DESC")
}

func (np *NormalPage) Order(column, defCol string) *NormalPage {
	if np.DB == nil {
		np.DB = DB()
	}
	val := np.Params.Get(column)
	if val != nil && val != "" {
		np.DB = np.DB.Order(val)
	} else {
		np.DB = np.DB.Order(defCol)
	}
	return np
}

func (np *NormalPage) ByPage(m, s interface{}) (*NormalPage, error) {
	db := np.DB.Model(m)
	var count int64
	db.Count(&count)
	limit, start := np.PageSetCount(count)
	err := db.Offset(start).Limit(limit).Find(s).Error
	np.PageSetList(s)
	return np, err
}
