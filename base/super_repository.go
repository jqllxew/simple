package base

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"reflect"
)

var br = SuperRepository{}

type SuperRepository struct{}

func (r SuperRepository) Create(db *gorm.DB, t interface{}) error {
	return db.Create(t).Error
}
func (r SuperRepository) Save(db *gorm.DB, t interface{}) error {
	return db.Save(t).Error
}
func (r SuperRepository) Update(db *gorm.DB, t interface{}) error {
	return db.Model(t).Update(t).Error
}
func (r SuperRepository) ById(db *gorm.DB, pk string, id, t interface{}) error {
	return db.First(t, fmt.Sprintf(whereId(id), pk, id)).Error
}
func (r SuperRepository) Delete(db *gorm.DB, pk string, id, t interface{}) error {
	return db.Delete(t, fmt.Sprintf(whereId(id), pk, id)).Error
}
func whereId(id interface{}) string {
	var f string
	if reflect.TypeOf(id).Name() == "string" {
		f = `%s=%q`
	} else {
		f = `%s=%v`
	}
	return f
}
