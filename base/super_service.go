package base

import (
	"gitee.com/jqllxew/simple"
)

type SuperService struct{}

func (s SuperService) Create(t interface{}) error {
	return br.Create(simple.DB(), t)
}
func (s SuperService) Save(t interface{}) error {
	return br.Save(simple.DB(), t)
}
func (s SuperService) Update(t interface{}) error {
	return br.Update(simple.DB(), t)
}
func (s SuperService) ById(pk string, id, t interface{}) error {
	return br.ById(simple.DB(), pk, id, t)
}
func (s SuperService) Delete(pk string, id, t interface{}) error {
	return br.Delete(simple.DB(), pk, id, t)
}
