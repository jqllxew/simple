package simple

import (
	"database/sql"
	"errors"
	"gitee.com/jqllxew/simple/utils"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/kataras/golog"
	"reflect"
	"time"
)

var db *gorm.DB

func OpenMySql(url string, maxIdleConns, maxOpenConns int, enableLog bool, models ...interface{}) (err error) {
	return OpenDB("mysql", url, maxIdleConns, maxOpenConns, enableLog, models...)
}
func OpenDB(dialect string, url string, maxIdleConns, maxOpenConns int, enableLog bool, models ...interface{}) (err error) {
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return defaultTableName
	}
	if db, err = gorm.Open(dialect, url); err != nil {
		golog.Errorf("opens database failed: %s", err.Error())
		return
	}
	db.LogMode(enableLog)
	db.SingularTable(true) // 禁用表名负数
	db.DB().SetMaxIdleConns(maxIdleConns)
	db.DB().SetMaxOpenConns(maxOpenConns)
	if err = db.AutoMigrate(models...).Error; nil != err {
		golog.Errorf("auto migrate tables failed: %s", err.Error())
	}
	return
}

// 获取数据库链接
func DB() *gorm.DB {
	return db
}

// 关闭连接
func CloseDB() {
	if db == nil {
		return
	}
	if err := db.Close(); nil != err {
		golog.Errorf("Disconnect from database failed: %s", err.Error())
	}
}

// 事务环绕
func Tx(db *gorm.DB, txFunc func(tx *gorm.DB) error) (err error) {
	tx := db.Begin()
	if tx.Error != nil {
		return
	}
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			panic(r) // re-throw panic after Rollback
		} else if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit().Error
		}
	}()
	err = txFunc(tx)
	return err
}
func RawsToMap(rows *sql.Rows) ([]map[string]interface{}, error) {
	if rows == nil {
		return nil, errors.New("rows is nil")
	}
	columns, er := rows.Columns()
	if er != nil {
		golog.Error(er)
		return nil, er
	}
	cache := make([]interface{}, len(columns))
	for index := range cache {
		var a interface{}
		cache[index] = &a
	}
	var list []map[string]interface{}
	for rows.Next() {
		_ = rows.Scan(cache...)
		item := make(map[string]interface{})
		for i, data := range cache {
			item[columns[i]] = *data.(*interface{})
		}
		list = append(list, item)
	}
	//将byte array转为string
	for index := range list {
		for _, column := range columns {
			val := list[index][column]
			if val != nil {
				ty := reflect.TypeOf(val).String()
				if ty == "[]uint8" {
					list[index][column] = utils.Uints2String(val.([]uint8))
				} else if ty == "time.Time" {
					list[index][column] = Time(val.(time.Time))
				}
			}
		}
	}
	return list, nil
}
