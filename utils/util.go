package utils

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"golang.org/x/text/encoding/simplifiedchinese"
	"hash"
	"strings"
)

func Md5(data []byte, secret string) string {
	return HashStr("md5", secret, data)
}
func Sha1(data []byte, secret string) string {
	return HashStr("sha1", secret, data)
}
func HashStr(eType, secret string, data []byte) string {
	var h hash.Hash
	if eType == "sha1" {
		h = sha1.New()
	} else if eType == "sha256" {
		h = sha256.New()
	} else {
		h = md5.New()
	}
	if secret != "" {
		h.Write([]byte(string(data) + "[" + secret + "]"))
	} else {
		h.Write(data)
	}
	return hex.EncodeToString(h.Sum(nil))
}

func Byte2String(byte []byte, charset string) string {
	var str string
	switch charset {
	case "GB18030":
		var decodeBytes, _ = simplifiedchinese.GB18030.NewDecoder().Bytes(byte)
		str = string(decodeBytes)
	case "UTF-8":
		fallthrough
	default:
		str = string(byte)
	}
	return str
}
func Uints2String(bs []uint8) string {
	var ba []byte
	for _, b := range bs {
		ba = append(ba, b)
	}
	return string(ba)
}
func UrlJoin(parts ...string) string {
	sep := "/"
	var ss []string
	for i, part := range parts {
		part = strings.TrimSpace(part)
		var (
			from = 0
			to   = len(part)
		)
		if strings.Index(part, sep) == 0 {
			from = 1
		}
		if strings.LastIndex(part, sep) == len(part)-1 {
			to = len(part) - 1
		}
		part = part[from:to]

		ss = append(ss, part)
		if i != len(parts)-1 {
			ss = append(ss, sep)
		}
	}
	return strings.Join(ss, "")
}
