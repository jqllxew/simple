package simple

import (
	"gitee.com/jqllxew/simple/config"
	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
)

func Init(confPath string, irisHook func(app *iris.Application), models ...interface{}) {
	config.Init(confPath)
	InitLog()
	conf := config.GetSimple()
	err := OpenDB(conf.DBDialect, conf.DBUrl, conf.DBMaxIdle, conf.DBMaxOpen, conf.ShowSql, models...)
	if err != nil {
		golog.Error(err)
	}
	InitIris(irisHook)
}
