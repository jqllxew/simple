package simple

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"strings"
	"time"
)

const (
	TimeFormat    = "2006-01-02 15:04:05"
	TimeFormatYmd = "2006-01-02"
)

// --- time ---
type Time time.Time

func NewTime(timeStr string) *Time {
	t := &Time{}
	_ = t.UnmarshalTime(timeStr, "")
	return t
}
func (t *Time) UnmarshalJSON(data []byte) (err error) {
	err = t.UnmarshalTime(string(data), "")
	return
}
func (t *Time) UnmarshalTime(timeStr string, tFmt string) (err error) {
	if tFmt == "" {
		if ymd := strings.Contains(timeStr, `-`); ymd && strings.Contains(timeStr, `:`) {
			tFmt = TimeFormat
		} else if ymd {
			tFmt = TimeFormatYmd
		} else {
			if strings.ReplaceAll(timeStr, `"`, ``) != "" {
				err = errors.New("时间格式错误")
			}
			return
		}
	}
	if strings.Contains(timeStr, `"`) {
		tFmt = `"` + tFmt + `"`
	}
	t0, err := time.ParseInLocation(tFmt, timeStr, time.Local)
	*t = Time(t0)
	return
}
func (t Time) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, len(TimeFormat)+2)
	if t.String() == "0001-01-01 00:00:00" {
		b = append(b, '"')
		b = append(b, '"')
		return b, nil
	}
	b = append(b, '"')
	b = time.Time(t).AppendFormat(b, TimeFormat)
	b = append(b, '"')
	return b, nil
}
func (t Time) String() string {
	return time.Time(t).Format(TimeFormat)
}
func (t *Time) Scan(value interface{}) error {
	var err error
	if t0, ok := value.(time.Time); ok {
		*t = Time(t0)
	} else if str, ok := value.(string); ok {
		err = t.UnmarshalTime(str, TimeFormat)
	} else {
		err = errors.New(fmt.Sprint("Failed to unmarshal time value:", value))
	}
	return err
}
func (t Time) Value() (driver.Value, error) {
	s := t.String()
	if s == "0001-01-01 00:00:00" {
		return nil, nil
	}
	return s, nil
}
