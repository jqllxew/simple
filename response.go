package simple

import (
	"github.com/kataras/golog"
	"github.com/kataras/iris/v12"
)

//https://github.com/swaggo/swag
type ResultData struct {
	Code int64       `json:"code"` //状态
	Msg  string      `json:"msg"`  //消息
	Data interface{} `json:"data"` //返回数据
}

// 200 define
func ResultSuccess_(msg string) ResultData {
	return ResultSuccess(msg, nil)
}
func ResultSuccess(msg string, data interface{}) ResultData {
	return ResultData{
		Code: iris.StatusOK,
		Msg:  msg,
		Data: data,
	}
}

// 401 error define
func Unauthorized(msg string, data interface{}) ResultData {
	golog.Errorf("Unauthorized: %s", msg)
	return ResultData{
		Code: iris.StatusUnauthorized,
		Msg:  msg,
		Data: data,
	}
}

// common error define
func ResultError(msg string) ResultData {
	golog.Errorf("error: %s", msg)
	return ResultData{
		Code: iris.StatusBadRequest,
		Msg:  msg,
		Data: nil,
	}
}
