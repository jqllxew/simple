package simple

import (
	"github.com/kataras/golog"
	"github.com/robfig/cron"
)

var (
	Cr *cron.Cron
)

func StartCron(spec string, fs ...func()) error {
	if Cr == nil {
		Cr = cron.New()
	}
	var err error
	for _, f := range fs {
		err = Cr.AddFunc(spec, f)
		if err != nil {
			golog.Errorf("cron task: %s", err)
			break
		}
	}
	Cr.Start()
	return err
}

func StopCron() {
	if Cr != nil {
		Cr.Stop()
		golog.Info("cron task stop.")
	}
}
